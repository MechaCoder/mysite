import React, { Component } from 'react';

function NavLink(props) {

  // creates a dom elemnt for the nav and the link to the page
  return <li><a onClick={handleClick} href={props.link} >{props.linkTitle}</a></li>;
}

var handleClick = function(event, prop) {
}

function BuildNav(prop) {
  var nav = []

  for(var i = 0; i<prop.linkArray.length; i++){
    nav.push(NavLink(prop.linkArray[i]))
  }

  return nav
}

class Header extends Component{

  render(){

    var NavLinkArray = [
      // {linkTitle: "Contact", link:"contact"},
      {linkTitle: "Blog", link:"blog"}
    ]

    return(
      <div className="header">
        <a href="/"><h2>MattyP Site</h2></a>
        <nav>
          <ol>
            {
              BuildNav({linkArray: NavLinkArray})
            }
          </ol>
        </nav>
      </div>
    )
  }
}

export default Header;
