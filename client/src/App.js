import React, { Component } from 'react';
var Entities = require('html-entities').XmlEntities;
var entities = new Entities();

function makeSection(props){

  switch (props.type) {
    case 'title':
      return(<h3 className="contentTitle">{props.text}</h3>);
    case 'sub-title':
      return(<h4 className="contentSubtitle">{props.text}</h4>)
    default:
      return(<span>{props.text}</span>)

  }
}

function ContentElement(props){

  var html = []

  for(var i = 0; i<props.body.length; i++){
    html.push(makeSection({type: props.body[i]['section-type'], text: props.body[i].content }))
  }

  return(<span>{html}</span>)
}

function makeBlogElement(props){
  return(<div>
    <a href={ props.contentDiv.URL}><h5>{ entities.decode(props.contentDiv.title.replace(/(<([^>]+)>)/ig,"")) }</h5></a>
    <span>{ entities.decode(props.contentDiv.excerpt.replace(/(<([^>]+)>)/ig,"").substring(1, 150)) }</span>
    <hr />
  </div>)
}

function BlogPosts(props) {

  console.log(props.body.posts)

  var html = [];

  for (var i = 0; i<props.body.posts.length; i++){
    html.push(makeBlogElement({contentDiv: props.body.posts[i]}))
  }


  return(<span>{html}</span>)

}

class Contentprocessor extends Component {
  constructor(props){
    super(props);
    this.state = {
      body: this.props.page,
      page: this.props.local
    }
    console.log(this.state)
  }
    render(){
      if(this.state.page === 'index'){
        return(<div><ContentElement body={this.state.body} /></div>)
    }
    else if (this.state.page === 'blog'){
      return(<div><BlogPosts body={this.state.body} /></div>)
    }
  }
}

class App extends Component {

  render() {

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
    }
    var httpURL = 'http://127.0.0.1:5000/'

    if(this.props.currentlocation === 'index' ){
      httpURL += 'intro/'
    } else if (this.props.currentlocation === 'blog'){
      httpURL = 'https://public-api.wordpress.com/rest/v1.1/sites/postitnoteninja.wordpress.com/posts'
    }


    xhttp.open("GET", httpURL, false);
    xhttp.send();

    return (
      <div id={this.props.currentlocation} className="App">
        <Contentprocessor local={this.props.currentlocation} page={ JSON.parse(xhttp.responseText) } />
      </div>
    );
  }
}

export default App;
