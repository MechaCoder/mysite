import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Header from './header';
import './index.css';

// const serverAddr = "http://127.0.0.1:8080"

ReactDOM.render(
  <Header />,
  document.getElementById('headerDiv')
)

var currlocal = 'index'
console.log(window.location.pathname);
if(window.location.pathname === '/blog'){
  console.log("running a blog")
  currlocal = 'blog'
}

ReactDOM.render(
  <App currentlocation={currlocal} />,
  document.getElementById('root')
);
