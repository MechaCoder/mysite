from pathlib import Path
from yamlcfg import YamlConfig


class File_handler():

    def __init__(self, directory=".", file_name="json.json", read_only=False):
        """ will go get the file if dose not exist and read_only is False the
            the file is created
         """
        self.fileL = "{}/{}".format(directory, file_name)

        if Path(self.fileL).is_file() is False:
            self.writefile('')
        pass

    def readFile(self):
        try:
            with open(self.fileL, mode='r', encoding='utf-8') as file_temp:
                return {
                    'sucsses': True,
                    'dataObj': {'file content': file_temp.read()}
                }
        except Exception as e:
            return {
                'sucsses': False,
                'dataObj': e
            }
        pass

    def writefile(self, content):
        try:
            with open(self.fileL, mode='w') as file_temp:
                file_temp.write(content)
                return {
                    'sucsses': True,
                    'dataObj': {}
                }
            pass
        except Exception as e:
            return {
                'sucsses': False,
                'dataObj': e
            }


class Ymalhandler():

    def __init__(self, path: object = './siteconfig.yml'):

        self.siteymalPath = path

    def getYmal(self):
        """returns an object that consits of atrubutes of the ymal objects.
        """
        return YamlConfig(self.siteymalPath).__dict__
