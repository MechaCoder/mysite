import json

import dataObjects.filestuff
import dataObjects.utils


def getReadme():
    """ is get the API's readme file and outputed"""

    mdfile = dataObjects.filestuff.File_handler('.', 'readme.md')
    s = mdfile.readFile()
    return json.dumps(s['dataObj']['file content'])


def get_intro_page():
    configfile = dataObjects.filestuff.Ymalhandler()
    return json.dumps(configfile.getYmal()['_data']['site']['intro_page'])


def getBlogPosts():
    """ this will get all post from the wordpress.com """
    configfile = dataObjects.filestuff.Ymalhandler()
    blogident = configfile.getYmal()['_data']['apiInformation']['wordpress']
    return blogident

def getallpages():
    """ this will get all page titles as defined in the datasourse """
    configfile = dataObjects.filestuff.Ymalhandler()
    temp = configfile.getYmal()['_data']['site'].keys()
    temp = dataObjects.utils.removeFromListByName(list(temp), 'intro_page')
    return json.dumps(temp)
