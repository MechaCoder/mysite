from flask import Flask
from flask_cors import CORS, cross_origin
from markdown import markdown


from dataObjects import getReadme
from dataObjects import get_intro_page
from dataObjects import getBlogPosts
from dataObjects import getallpages

app = Flask(__name__)

CORS(app)


@app.route("/")
def get_readme():
    return markdown(getReadme())


@app.route("/intro/")
def get_intro():
    return get_intro_page()


@app.route("/wordpress/")
def get_wp():
    return getBlogPosts()


@app.route("/sitenav/")
def get_nav_pages():
    return getallpages()

if __name__ == '__main__':
    app.run(debug=True)
