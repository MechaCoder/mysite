import unittest
import os
import dataObjects


class Base_test(unittest.TestCase):

    def tearDown(self):
        os.remove("{}/{}".format('.', 'test.json'))
        pass

    def exsameObject(self, obj):

        self.assertEqual(
            type(obj),
            type({}),
            'is the object a dict'
        )

        self.assertEqual(
            obj.keys(),
            {'sucsses': True, 'dataObj': {}}.keys(),
            'dose the object keys'
        )

        self.assertEqual(
            type(obj['sucsses']),
            type(True),
            'is the sucsses key is a bool'
        )

        self.assertEqual(
            type(obj['dataObj']),
            type({}),
            'is the dataObj key is a dict'
        )


class TestFileHandler(Base_test):

    def test_file_write(self):
        fileObj = dataObjects.filestuff.File_handler('.', 'test.json')
        testObj = fileObj.writefile('...')
        self.exsameObject(testObj)
        pass

    def test_file_read(self):
        fileObj = dataObjects.filestuff.File_handler('.', 'test.json')
        testObj = fileObj.readFile()
        self.exsameObject(testObj)


class TestData(Base_test):

    def tearDown(self):
        pass

    def test_getReadme(self):
        temp = dataObjects.getReadme()
        self.assertEqual(type(temp), type(''))

    def test_get_intro_page(self):
        temp = dataObjects.get_intro_page()
        self.assertEqual(type(temp), type(''))
        pass

    def test_get_getBlogPosts(self):
        temp = dataObjects.getBlogPosts()
        self.assertEqual(type(temp), type(''))

    def test_get_getBlogPosts(self):
        temp = dataObjects.getBlogPosts()
        self.assertEqual(type(temp), type(''))

    def test_get_getallpages(self):
        temp = dataObjects.getallpages()
        self.assertEqual(type(temp), type(''))

if __name__ == '__main__':
    unittest.main()
